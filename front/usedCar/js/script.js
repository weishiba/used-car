$(function () {
    //异步登录提示
    $("#loginForm").submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: "http://localhost:8888/usedcar/user/login",
            type: "post",
			dataType:"json",
            data: $(this).serialize(),
            success: function (response) {
                if (response.status == 200) {
					console.log(response);
					document.cookie = response.data.token;
                    //登录成功,跳转到主页面
                    window.location.href = "home.html?token="+response.data.token;
                } else if (response.status == 401) {
                    //用户名或密码不正确
                    $("#loginHint").html(response.msg);
                } else if (response.status == 400) {
                    //说明验证码不正确
                    $("#loginHint").html(response.msg);
                }
            }
        });
    });
    //异步注册
    $("#registerForm").submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: "http://localhost:8888/usedcar/user/register?token="+document.cookie.split(";")[0],
            type: "post",
            data: $(this).serialize(),
            success: function (data) {
                if (data == 1) {
                    //注册成功,跳转到登录页面
					alert("注册成功");
                    window.location.href ="login.html";
                } else if (data == 0) {
                    //用户名不正确
                    $("#rigisterHint").html("<b>用户名已被注册</b>");
                } else if (data == -1) {
                    //
                    $("#rigisterHint").html("<b>两次密码不一致</b>");
                }
            }
        });
    });
    $("#icon").click(function () {
        var type = $("#pwd1").attr("type");
        if (type == "password") {
            $("#icon").attr("class", "iconfont icon-showpwd");
            $("#pwd1").prop("type", "text");
        } else {
            $("#icon").attr("class", "iconfont icon-hiddenpwd");
            $("#pwd1").prop("type", "password");
        }
    });
    $("#icon1").click(function () {
        var type = $("#pwd2").attr("type");
        if (type == "password") {
            $("#icon1").attr("class", "iconfont icon-showpwd");
            $("#pwd2").prop("type", "text");
            $("#pwd3").prop("type", "text");
        } else {
            $("#icon1").attr("class", "iconfont icon-hiddenpwd");
            $("#pwd2").prop("type", "password");
            $("#pwd3").prop("type", "password");
        }
    });
	$("#logOut").click(function () {
		console.log("logout-start");
	    $.ajax({
	        url: "http://localhost:8888/usedcar/user/logout?token="+document.cookie.split(";")[0],
	        type: "post",
	        success: function (data) {
	            if (data.status == 200) {
					var cookies = document.cookie.split(";");
					    for (var i = 0; i < cookies.length; i++) {
					        var cookie = cookies[i];
					        var eqPos = cookie.indexOf("=");
					        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
					        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
					    }
					    if(cookies.length > 0)
					    {
					        for (var i = 0; i < cookies.length; i++) {
					            var cookie = cookies[i];
					            var eqPos = cookie.indexOf("=");
					            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
					            var domain = location.host.substr(location.host.indexOf('.'));
					            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/; domain=" + domain;
					        }
					    }
	                window.location.href = "login.html";
	            }
	        }
	    });
	});
});


function go(pageNow) {
    $("#datas").load(base + "/permission/book/list?pageNow=" + pageNow + "&searchName=" + $("#searchName").val());
}


function list() {
    window.location.href = base + "/permission/book/index";
}

function uploadImg(id) {
    window.location.href = base + "/permission/book/uploadImg?id=" + id;
}

function login() {
    window.location.href = "/usedcar/user/index";
}

function register() {
    window.location.href = "/usedcar/user/register";
}

function editBook(id) {
    window.location.href = base + "/permission/book/editBook?info=2&id=" + id;
}

function deleteBook(id) {
    if (confirm("确定要删除这本书吗？")) {
        window.location.href = base + "/permission/book/deleteBook?id=" + id;
    } else {
        window.location.href = base + "/permission/book/index";
    }
}

//购买图书
function buyBook(id, uid) {
    window.location.href = base + "/permission/book/buyBook?id=" + id + "&uid  =" + uid;
}

function addBook() {
    window.location.href = base + "/permission/book/addBook?info=1";
}



function leave() {
    alert("谢谢使用，已安全退出。");
    window.location.href = "/usedcar/user/leave";
}

